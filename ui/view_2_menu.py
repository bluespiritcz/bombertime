import glob
import arcade
import arcade.gui

import config
from logic import bombertime
from logic.bombertime import Game
from logic.controller import SecondMenuController
from ui.buttons import ConfirmButton, NavigationButton
from ui.view_3_game import GameView


class SecondMenuView(arcade.View):
    """
    Arena options menu.
    arenas are saved in arenas/ directory with filename format "arena_[NUM]_[ARENA_NAME].txt"
    arena structure legend:
        -       blank field
        B       brick
        X       solid
    """

    def __init__(self, start_view):
        super().__init__()

        self.controller = SecondMenuController(self)

        self.ui_manager = arcade.gui.UIManager(self.window)
        self.ui_manager.enable()

        self.start_view = start_view

        self.arenas_list = []
        self.arena_index = None

        self.files_list = glob.glob("resources/arenas/arena_*.txt")
        for filename in self.files_list:
            filename_txt = filename.split("/", 1)[1]
            filename_txt = filename_txt.split("\\", 1)[1]
            arena_title = filename_txt.split(".txt")[0]
            self.arenas_list.append(arena_title)

        self.arena_preview = ArenaPreview()

        self.setup()

    def setup(self, arena_index=0):
        self.arena_index = arena_index
        bombertime.settings.set_arena_name(self.arenas_list[self.arena_index])
        self.arena_preview.set_arena(self.arenas_list[self.arena_index])

        arcade.set_background_color(arcade.color.BLACK)
        self.v_box = arcade.gui.UIBoxLayout()

        # Title
        title_label = arcade.gui.UITextArea(text="Choose arena",
                                            font_name=config.MAIN_FONT_NAME,
                                            font_size=config.SIZE * 0.4,
                                            text_color=arcade.color.ORANGE,
                                            multiline=False)
        title_label.fit_content()
        self.v_box.add(title_label.with_space_around(top=config.SIZE, bottom=config.SIZE))

        # Arena label
        self.arena_label = arcade.gui.UILabel(text=self.arenas_list[self.arena_index].split("_")[2], font_name=config.MAIN_FONT_NAME,
                                              width=config.SCREEN_WIDTH, align="center")
        self.v_box.add(self.arena_label.with_space_around(bottom=config.SIZE // 2))

        # Buttons with arena preview
        self.h_box = arcade.gui.UIBoxLayout(vertical=False)
        btn_left = NavigationButton("<", config.SCREEN_WIDTH // 3, config.SCREEN_HEIGHT // 2,
                                    config.SIZE // 2, config.SIZE // 2,
                                    self.switch_arena_left)
        self.h_box.add(btn_left.with_space_around(right=config.SIZE // 2))

        self.arena_preview_UI = arcade.gui.UISpriteWidget(width=config.SIZE * 2, height=config.SIZE * 2,
                                                          sprite=self.arena_preview)
        self.h_box.add(self.arena_preview_UI)

        btn_right = NavigationButton(">", config.SCREEN_WIDTH - config.SCREEN_WIDTH // 3,
                                     config.SCREEN_HEIGHT // 2,
                                     config.SIZE // 2, config.SIZE // 2,
                                     self.switch_arena_right)
        self.h_box.add(btn_right.with_space_around(left=config.SIZE // 2))

        self.v_box.add(self.h_box)

        self.btn_confirm = ConfirmButton(text="Start game",
                                         width=config.SIZE * 3,
                                         height=int(config.SIZE * 0.6),
                                         action_function=self.start_game,
                                         game_settings=bombertime.settings)
        self.v_box.add(self.btn_confirm.with_space_around(top=config.SIZE))

        # Help text
        self.v_box_help = arcade.gui.UIBoxLayout()
        help_text_1 = arcade.gui.UILabel(text="Use arrows to change arena type,",
                                         font_name=config.MAIN_FONT_NAME,
                                         font_size=config.SIZE * 0.15,
                                         width=config.SCREEN_WIDTH,
                                         align="center")
        help_text_2 = arcade.gui.UILabel(text="press ENTER to start game",
                                         font_name=config.MAIN_FONT_NAME,
                                         font_size=config.SIZE * 0.15,
                                         width=config.SCREEN_WIDTH,
                                         align="center")
        help_text_3 = arcade.gui.UILabel(text="or press ESC to get back",
                                         font_name=config.MAIN_FONT_NAME,
                                         font_size=config.SIZE * 0.15,
                                         width=config.SCREEN_WIDTH,
                                         align="center")

        self.v_box_help.add(help_text_1)
        self.v_box_help.add(help_text_2.with_space_around(top=config.SIZE * 0.1))
        self.v_box_help.add(help_text_3.with_space_around(top=config.SIZE * 0.1))
        self.v_box.add(self.v_box_help.with_space_around(top=config.SIZE, bottom=config.SIZE // 4))

        self.ui_manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )

    def arena_index_update_ui(self):
        arena_name = self.arenas_list[self.arena_index]

        self.arena_preview.set_arena(arena_name)

        self.arena_label.text = arena_name.split("_")[2]

    def start_game(self):
        bombertime.settings.set_arena_name(self.arenas_list[self.arena_index])

        game_logic = Game()
        game_view = GameView(game_logic)
        self.window.show_view(game_view)

    def back_to_main_screen(self):
        self.window.show_view(self.start_view)

    def switch_arena_left(self):
        if self.arena_index > 0:
            self.arena_index -= 1
        else:
            self.arena_index = len(self.arenas_list) - 1
        self.arena_index_update_ui()

    def switch_arena_right(self):
        if self.arena_index < len(self.arenas_list) - 1:
            self.arena_index += 1
        else:
            self.arena_index = 0
        self.arena_index_update_ui()

    def on_draw(self):
        arcade.start_render()
        self.ui_manager.draw()

    def on_hide_view(self):
        self.ui_manager.disable()

    def on_key_release(self, key, modifiers):
        self.controller.on_key_release(key)


class ArenaPreview(arcade.Sprite):
    def __init__(self):
        super().__init__()

        self.center_x = config.SCREEN_WIDTH // 2
        self.center_y = config.SCREEN_HEIGHT // 2
        self.scale = config.ARENA_PREVIEW_SCALING

    def set_arena(self, arena_name):
        self.texture = arcade.load_texture("resources/arenas/previews/" + arena_name + ".png")
