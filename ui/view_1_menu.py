import arcade
import arcade.gui

import config
from logic import bombertime
from logic.controller import FirstMenuController
from ui.buttons import NavigationButton, ConfirmButton
from ui.view_2_menu import SecondMenuView


class FirstMenuView(arcade.View):
    """
    Introduction menu with game mode options and number of rounds settings.

    Number of rounds switcher: 1, 3, 5, 7

    When player sets the number of rounds (or keep it on 1)
    and clicks on any game mode option
    it redirects him to the screen with map options.
    """

    def __init__(self):
        super().__init__()

        self.controller = FirstMenuController(self)

        self.ui_manager = arcade.gui.UIManager(self.window)
        self.ui_manager.enable()

        self.num_of_rounds_index = None
        self.cup = False

        self.setup()

    def setup(self, num_of_rounds_index=0):
        # Number of rounds logic
        self.num_of_rounds_index = num_of_rounds_index

        # Background
        arcade.set_background_color(arcade.color.BLACK)

        self.v_box = arcade.gui.UIBoxLayout()

        # Title
        title_label = arcade.gui.UITextArea(text="BomberTIME!",
                                            font_name=config.MAIN_FONT_NAME,
                                            font_size=config.SIZE * 0.6,
                                            text_color=arcade.color.ORANGE)
        title_label.fit_content()
        self.v_box.add(title_label.with_space_around(top=config.SIZE * 2))

        # Game mode - buttons
        self.btn_classic_mode = ConfirmButton(text="Classic battle",
                                              width=config.SIZE * 5,
                                              height=int(config.SIZE * 0.6),
                                              action_function=self.go_to_next_screen,
                                              game_settings=bombertime.Settings("classic",
                                                                                config.NUM_OF_ROUNDS_LIST[
                                                                                    self.num_of_rounds_index], 2))
        self.v_box.add(self.btn_classic_mode.with_space_around(top=config.SIZE * 2))

        # Number of rounds - labels
        num_of_rounds_title_label = arcade.gui.UILabel(text="Number of rounds:",
                                                       font_name=config.MAIN_FONT_NAME)
        self.v_box.add(num_of_rounds_title_label.with_space_around(top=config.SIZE))

        self.num_of_rounds_label = arcade.gui.UILabel(
            text=str(config.NUM_OF_ROUNDS_LIST[self.num_of_rounds_index]),
            font_name=config.MAIN_FONT_NAME)

        # Number of rounds - switching buttons
        self.btn_left = NavigationButton("<", config.SCREEN_WIDTH // 3, config.SCREEN_HEIGHT // 6,
                                         config.SIZE // 2, config.SIZE // 2, self.switch_num_of_rounds_left)
        self.btn_right = NavigationButton(">", config.SCREEN_WIDTH - config.SCREEN_WIDTH // 3,
                                          config.SCREEN_HEIGHT // 6,
                                          config.SIZE // 2, config.SIZE // 2, self.switch_num_of_rounds_right)

        self.h_box = arcade.gui.UIBoxLayout(vertical=False)
        self.h_box.add(self.btn_left.with_space_around(right=config.SIZE // 2))
        self.h_box.add(self.num_of_rounds_label)
        self.h_box.add(self.btn_right.with_space_around(left=config.SIZE // 2))
        self.v_box.add(self.h_box.with_space_around(top=config.SIZE * 0.2))

        # Help text
        self.v_box_help = arcade.gui.UIBoxLayout()
        help_text_1 = arcade.gui.UILabel(text="Use arrows to change number of rounds",
                                         font_name=config.MAIN_FONT_NAME,
                                         font_size=config.SIZE * 0.15,
                                         width=config.SCREEN_WIDTH,
                                         align="center")
        help_text_2 = arcade.gui.UILabel(text="or press ENTER to confirm",
                                         font_name=config.MAIN_FONT_NAME,
                                         font_size=config.SIZE * 0.15,
                                         width=config.SCREEN_WIDTH,
                                         align="center")
        self.v_box_help.add(help_text_1)
        self.v_box_help.add(help_text_2.with_space_around(top=config.SIZE * 0.1))
        self.v_box.add(self.v_box_help.with_space_around(top=config.SIZE, bottom=config.SIZE))

        self.ui_manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )

    def num_of_rounds_update_ui(self):
        num_of_rounds = config.NUM_OF_ROUNDS_LIST[self.num_of_rounds_index]

        self.btn_classic_mode.game_settings.set_num_of_rounds(num_of_rounds)

        self.num_of_rounds_label.text = str(num_of_rounds)
        self.num_of_rounds_label.trigger_render()

    def go_to_next_screen(self):
        setup_view = SecondMenuView(self)
        self.window.show_view(setup_view)

    def switch_num_of_rounds_left(self):
        if self.num_of_rounds_index > 0:
            self.num_of_rounds_index -= 1
        else:
            self.num_of_rounds_index = len(config.NUM_OF_ROUNDS_LIST) - 1

        self.num_of_rounds_update_ui()

    def switch_num_of_rounds_right(self):
        if self.num_of_rounds_index < len(config.NUM_OF_ROUNDS_LIST) - 1:
            self.num_of_rounds_index += 1
        else:
            self.num_of_rounds_index = 0

        self.num_of_rounds_update_ui()

    def on_draw(self):
        arcade.start_render()
        self.ui_manager.draw()

    def on_key_release(self, key, modifiers):
        self.controller.on_key_release(key)
