import arcade
import arcade.gui

import config
import ui.view_1_menu
from logic.controller import GameOverMenuController
from ui.buttons import NavigationButton


class GameOverView(arcade.View):
    def __init__(self, result, game_view=None):
        super().__init__()

        self.controller = GameOverMenuController(self)

        self.game_view = game_view
        self.winner = result.winner
        self.result = result
        self.time_taken = result.time_taken

        self.ui_manager = arcade.gui.UIManager(self.window)
        self.ui_manager.enable()

        self.v_box = arcade.gui.UIBoxLayout()
        self.h_box = arcade.gui.UIBoxLayout(vertical=False)

        # Title
        title_label = arcade.gui.UITextArea(text="GAME OVER!",
                                            font_name=config.MAIN_FONT_NAME,
                                            font_size=config.SIZE * 0.6,
                                            text_color=arcade.color.ORANGE,
                                            multiline=False)
        title_label.fit_content()
        self.v_box.add(title_label.with_space_around(top=config.SIZE * 0.6))

        # Player scores
        if self.winner is None:
            result_label = arcade.gui.UITextArea(text="It was a DRAW!",
                                                 font_name=config.MAIN_FONT_NAME,
                                                 font_size=18,
                                                 text_color=arcade.color.WHITE,
                                                 multiline=False)
        else:
            if self.winner.player_color == "red":
                color = arcade.color.BITTERSWEET
            elif self.winner.player_color == "blue":
                color = arcade.color.BLUEBERRY
            result_label = arcade.gui.UITextArea(text=self.winner.player_color.upper() + " player is the winner!",
                                                 font_name=config.MAIN_FONT_NAME,
                                                 font_size=18,
                                                 text_color=color,
                                                 multiline=False)

        red_score_value = str(self.game_view.game.cup.results_dict["red"])
        red_score = arcade.gui.UILabel(text=red_score_value,
                                       font_name=config.MAIN_FONT_NAME,
                                       font_size=config.SIZE * 0.5,
                                       text_color=arcade.color.BITTERSWEET)
        score_delimiter = arcade.gui.UILabel(text=":",
                                             font_name=config.MAIN_FONT_NAME,
                                             font_size=config.SIZE * 0.5,
                                             text_color=arcade.color.WHITE)
        blue_score = arcade.gui.UILabel(text=str(self.game_view.game.cup.results_dict["blue"]),
                                        font_name=config.MAIN_FONT_NAME,
                                        font_size=config.SIZE * 0.5,
                                        text_color=arcade.color.BLUEBERRY)

        result_label.fit_content()
        self.v_box.add(result_label.with_space_around(top=config.SIZE * 0.5, bottom=config.SIZE * 0.5))

        self.h_box.add(red_score.with_space_around(right=config.SIZE // 2))
        self.h_box.add(score_delimiter)
        self.h_box.add(blue_score.with_space_around(left=config.SIZE // 2))
        self.v_box.add(self.h_box)

        # Buttons
        btn_continue = NavigationButton(text="Restart",
                                        center_x=config.SCREEN_WIDTH // 3,
                                        center_y=config.SCREEN_HEIGHT // 6,
                                        width=config.SIZE * 4,
                                        height=int(config.SIZE * 0.6),
                                        action_function=self.restart_game)
        self.v_box.add(btn_continue.with_space_around(top=config.SIZE * 0.5))

        btn_exit = NavigationButton(text="Exit",
                                    center_x=config.SCREEN_WIDTH // 3,
                                    center_y=config.SCREEN_HEIGHT // 6,
                                    width=config.SIZE * 4,
                                    height=int(config.SIZE * 0.6),
                                    action_function=self.exit_to_main_menu)
        self.v_box.add(btn_exit.with_space_around(top=config.SIZE * 0.2))

        # Help
        self.v_box_help = arcade.gui.UIBoxLayout()
        help_text_1 = arcade.gui.UILabel(text="Press TAB to restart game",
                                         font_name=config.MAIN_FONT_NAME,
                                         font_size=config.SIZE * 0.15,
                                         width=config.SCREEN_WIDTH,
                                         align="center")
        help_text_2 = arcade.gui.UILabel(text="or ESC to exit",
                                         font_name=config.MAIN_FONT_NAME,
                                         font_size=config.SIZE * 0.15,
                                         width=config.SCREEN_WIDTH,
                                         align="center")
        self.v_box_help.add(help_text_1)
        self.v_box_help.add(help_text_2.with_space_around(top=config.SIZE * 0.1))
        self.v_box.add(self.v_box_help.with_space_around(top=config.SIZE * 2))

        self.ui_manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )

    def on_draw(self):
        arcade.start_render()
        arcade.set_background_color(arcade.color.BLACK)
        self.ui_manager.draw()

    def on_key_release(self, key, modifiers):
        self.controller.on_key_release(key)

    def restart_game(self):
        self.game_view.game.setup()
        self.window.show_view(self.game_view)

    def exit_to_main_menu(self):
        start_view = ui.view_1_menu.FirstMenuView()
        self.window.show_view(start_view)
