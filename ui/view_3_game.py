from time import sleep

import arcade
import arcade.gui

import config
from logic.bombertime import GameResult
from logic.controller import Controller
from ui.view_4_round_over import RoundOverView


class GameView(arcade.View):
    def __init__(self, game_logic):
        super().__init__()

        self.game = game_logic
        self.controller = Controller(self.game)

        self.scoreboard = Scoreboard(self.game)

    def on_draw(self):
        # This command has to happen before we start drawing
        arcade.start_render()

        # Draw scoreboard
        self.scoreboard.draw()

        # Draw arena
        self.game.arena.draw()

        # Draw all sprites
        self.game.all_bombs_list.draw()
        self.game.explosion_list.draw()
        self.game.item_list.draw()
        self.game.player_list.draw()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if len(self.game.player_list) < 2:
            return
        self.controller.on_key_press(key)

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        if len(self.game.player_list) < 2:
            return
        self.controller.on_key_release(key)

    def on_update(self, delta_time):
        if self.game.game_over and len(self.game.explosion_list) == 0:
            game_result = GameResult(self.game.set_winner(), self.game.time_taken)

            if game_result.winner is not None:
                self.game.cup.results_dict[game_result.winner.player_color] += 1

            self.game.cup.time_taken += game_result.time_taken
            round_over_view = RoundOverView(game_result, self)

            sleep(1)
            self.window.show_view(round_over_view)
            self.window.set_mouse_visible(True)

        self.game.on_update(delta_time)
        self.scoreboard.on_update()


class Scoreboard:
    def __init__(self, game):
        self.game = game

        self.player_controls_labels = []
        self.player_icon_list = arcade.SpriteList(use_spatial_hash=True)
        self.player_stats_labels = []
        self.player_stats_value_dict = {}

        for num in range(2):
            if num == 0:
                player_controls_text = "WSAD\n+\nSPACE"
            elif num == 1:
                player_controls_text = "↑↓←→\n+\nENTER"
            player_controls_label = arcade.Text(text=player_controls_text,
                                                start_x=config.SCOREBOARD_CONTROLS_START_X[num],
                                                start_y=config.SCOREBOARD_CONTROLS_START_Y,
                                                anchor_x=config.SCOREBOARD_CONTROLS_ANCHOR_X[num],
                                                font_name=config.MAIN_FONT_NAME,
                                                font_size=config.SCOREBOARD_CONTROLS_FONT_SIZE,
                                                color=arcade.make_transparent_color(arcade.color.WHITE, 80),
                                                width=config.SIZE,
                                                multiline=True,
                                                align="center")
            self.player_controls_labels.append(player_controls_label)

            player_icon = PlayerIcon(num)
            self.player_icon_list.append(player_icon)

            flame_label = arcade.Text(text="Flame: ",
                                      start_x=config.SCOREBOARD_STAT_START_X[num],
                                      start_y=config.SCOREBOARD_STAT_START_Y,
                                      anchor_x=config.SCOREBOARD_STAT_ANCHOR_X[num],
                                      font_name=config.MAIN_FONT_NAME,
                                      font_size=config.SCOREBOARD_FONT_SIZE,
                                      color=arcade.color.WHITE)
            bombs_label = arcade.Text(text="Bombs: ",
                                      start_x=config.SCOREBOARD_STAT_START_X[num],
                                      start_y=flame_label.y - config.SCOREBOARD_STAT_SPACING,
                                      anchor_x=config.SCOREBOARD_STAT_ANCHOR_X[num],
                                      font_name=config.MAIN_FONT_NAME,
                                      font_size=config.SCOREBOARD_FONT_SIZE,
                                      color=arcade.color.WHITE)
            speed_label = arcade.Text(text="Speed: ",
                                      start_x=config.SCOREBOARD_STAT_START_X[num],
                                      start_y=bombs_label.y - config.SCOREBOARD_STAT_SPACING,
                                      anchor_x=config.SCOREBOARD_STAT_ANCHOR_X[num],
                                      font_name=config.MAIN_FONT_NAME,
                                      font_size=config.SCOREBOARD_FONT_SIZE,
                                      color=arcade.color.WHITE)

            self.player_stats_labels.extend([flame_label, bombs_label, speed_label])

            if num == 0:
                flame_label_value_spacing = flame_label.content_width
                bombs_label_value_spacing = bombs_label.content_width
                speed_label_value_spacing = speed_label.content_width
            else:
                flame_label_value_spacing = 0
                bombs_label_value_spacing = 0
                speed_label_value_spacing = 0

            flame_value = arcade.Text(text="",
                                      start_x=config.SCOREBOARD_STAT_START_X[num] + flame_label_value_spacing,
                                      start_y=config.SCOREBOARD_STAT_START_Y,
                                      font_name=config.MAIN_FONT_NAME,
                                      font_size=config.SCOREBOARD_FONT_SIZE,
                                      color=arcade.color.WHITE)
            bombs_value = arcade.Text(text="",
                                      start_x=config.SCOREBOARD_STAT_START_X[num] + bombs_label_value_spacing,
                                      start_y=flame_label.y - config.SCOREBOARD_STAT_SPACING,
                                      font_name=config.MAIN_FONT_NAME,
                                      font_size=config.SCOREBOARD_FONT_SIZE,
                                      color=arcade.color.WHITE)
            speed_value = arcade.Text(text="",
                                      start_x=config.SCOREBOARD_STAT_START_X[num] + speed_label_value_spacing,
                                      start_y=bombs_label.y - config.SCOREBOARD_STAT_SPACING,
                                      font_name=config.MAIN_FONT_NAME,
                                      font_size=config.SCOREBOARD_FONT_SIZE,
                                      color=arcade.color.WHITE)

            self.player_stats_value_dict["flame_stat_" + str(num)] = flame_value
            self.player_stats_value_dict["bombs_stat_" + str(num)] = bombs_value
            self.player_stats_value_dict["speed_stat_" + str(num)] = speed_value

        self.timer = arcade.Text(text="",
                                 start_x=config.SCOREBOARD_TIMER_START_X,
                                 start_y=config.SCOREBOARD_TIMER_START_Y,
                                 anchor_x="center",
                                 anchor_y="center",
                                 font_name=config.MAIN_FONT_NAME,
                                 font_size=config.SCOREBOARD_FONT_SIZE,
                                 color=arcade.color.WHITE)

    def draw(self):
        for label in self.player_controls_labels:
            label.draw()

        self.player_icon_list.draw()
        self.timer.draw()

        for label in self.player_stats_labels:
            label.draw()

        for stat in self.player_stats_value_dict.values():
            stat.draw()

    def on_update(self):
        for num in range(2):
            if self.game.player_dict[num] is None:
                flame = "x"
                bombs = "x"
                speed = "x"
            else:
                flame = self.game.player_dict[num].flame_length
                if flame == config.PLAYER_DEFAULT_MAX_FLAME_LENGTH:
                    flame = "MAX"
                    self.player_stats_value_dict.get("flame_stat_" + str(num)).color = arcade.color.ORANGE
                bombs = self.game.player_dict[num].bombs_available
                if bombs == config.PLAYER_DEFAULT_MAX_BOMBS_AVAILABLE:
                    bombs = "MAX"
                    self.player_stats_value_dict.get("bombs_stat_" + str(num)).color = arcade.color.ORANGE
                speed = self.game.player_dict[num].speed
                if speed == config.PLAYER_DEFAULT_MAX_SPEED:
                    speed = "MAX"
                    self.player_stats_value_dict.get("speed_stat_" + str(num)).color = arcade.color.ORANGE

            self.player_stats_value_dict.get("flame_stat_" + str(num)).value = str(flame)
            self.player_stats_value_dict.get("bombs_stat_" + str(num)).value = str(bombs)
            self.player_stats_value_dict.get("speed_stat_" + str(num)).value = str(speed)

        self.timer.value = round(config.ROUND_TIME_LIMIT - self.game.time_taken, 1)
        if float(self.timer.value) <= 50:
            self.timer.color = arcade.color.RED
            self.timer.font_size = config.SCOREBOARD_FONT_SIZE_BIGGER
        else:
            self.timer.color = arcade.color.WHITE
            self.timer.font_size = config.SCOREBOARD_FONT_SIZE


class PlayerIcon(arcade.Sprite):
    def __init__(self, num):
        super().__init__()

        # Texture
        self.texture = arcade.load_texture(config.PLAYER_ASSETS_FOLDER[num] + config.PLAYER_ICON)
        self.scale = config.PLAYER_ICON_SCALING

        # Position
        self.center_x = config.PLAYER_ICON_CENTER_X[num]
        self.center_y = config.PLAYER_ICON_CENTER_Y
