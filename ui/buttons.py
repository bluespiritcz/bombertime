import arcade.gui

import config
from logic import bombertime


class ConfirmButton(arcade.gui.UIFlatButton):
    def __init__(self, text, width, height, action_function, game_settings, style=config.BTN_DEFAULT_STYLE):
        super().__init__(text=text, width=width, height=height, style=style)
        self.action_function = action_function
        self.game_settings = game_settings

    def on_click(self, event):
        bombertime.settings = self.game_settings
        self.action_function()


class NavigationButton(arcade.gui.UIFlatButton):
    def __init__(self, text, center_x, center_y, width, height, action_function, style=config.BTN_DEFAULT_STYLE):
        super().__init__(text=text, x=center_x, y=center_y, width=width, height=height, style=style)
        self.action_function = action_function

    def on_click(self, event):
        self.action_function()
