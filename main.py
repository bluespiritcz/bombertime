import arcade

import config
from ui.view_1_menu import FirstMenuView

if __name__ == "__main__":
    window = arcade.Window(config.SCREEN_WIDTH, config.SCREEN_HEIGHT, config.SCREEN_TITLE)
    window.set_mouse_visible(True)
    view = FirstMenuView()
    window.show_view(view)
    arcade.run()
