import arcade

import config


class Controller:
    def __init__(self, game):
        self.game = game

    def on_key_press(self, key):
        """Called whenever a key is pressed. """
        for i in range(2):
            if key == config.PLAYER_CONTROLS_UP[i]:
                self.game.player_list[i].up_pressed = True
            elif key == config.PLAYER_CONTROLS_DOWN[i]:
                self.game.player_list[i].down_pressed = True
            elif key == config.PLAYER_CONTROLS_LEFT[i]:
                self.game.player_list[i].left_pressed = True
            elif key == config.PLAYER_CONTROLS_RIGHT[i]:
                self.game.player_list[i].right_pressed = True

    def on_key_release(self, key):
        """Called when the user releases a key. """
        for i in range(2):
            if key == config.PLAYER_CONTROLS_UP[i]:
                self.game.player_list[i].up_pressed = False
            if key == config.PLAYER_CONTROLS_DOWN[i]:
                self.game.player_list[i].down_pressed = False
            elif key == config.PLAYER_CONTROLS_LEFT[i]:
                self.game.player_list[i].left_pressed = False
            elif key == config.PLAYER_CONTROLS_RIGHT[i]:
                self.game.player_list[i].right_pressed = False
            elif key == config.PLAYER_CONTROLS_BOMB_PLACEMENT[i]:
                self.game.bomb_placement(i)


class FirstMenuController:
    def __init__(self, first_menu):
        self.menu = first_menu

    def on_key_release(self, key):
        if key == arcade.key.LEFT:
            self.menu.switch_num_of_rounds_left()
        elif key == arcade.key.RIGHT:
            self.menu.switch_num_of_rounds_right()
        elif key == arcade.key.ENTER:
            self.menu.btn_classic_mode.on_click(event=self.menu.go_to_next_screen)


class SecondMenuController:
    def __init__(self, second_menu):
        self.menu = second_menu

    def on_key_release(self, key):
        if key == arcade.key.LEFT:
            self.menu.switch_arena_left()
        elif key == arcade.key.RIGHT:
            self.menu.switch_arena_right()
        elif key == arcade.key.ENTER:
            self.menu.start_game()
        elif key == arcade.key.ESCAPE:
            self.menu.back_to_main_screen()


class RoundOverMenuController:
    def __init__(self, round_over_menu):
        self.menu = round_over_menu

    def on_key_release(self, key):
        if key == arcade.key.TAB:
            self.menu.continue_in_game()
        elif key == arcade.key.ESCAPE:
            self.menu.exit_to_main_menu()


class GameOverMenuController:
    def __init__(self, game_over_menu):
        self.menu = game_over_menu

    def on_key_release(self, key):
        if key == arcade.key.TAB:
            self.menu.restart_game()
        elif key == arcade.key.ESCAPE:
            self.menu.exit_to_main_menu()
