import glob

import arcade

import config
from logic.arena import Arena, Bomb, BombUp, Flame, Skate
from logic.player import Player

settings = None


class Game:
    def __init__(self):
        # Arena
        self.arena = None

        # Sprite lists
        self.player_list = None
        self.all_bombs_list = None
        self.explosion_list = None
        self.item_list = None

        # Sudden death
        self.sudden_death_timer = 0
        self.sudden_death_in_progress = False
        self.sudden_death_row = 0
        self.sudden_death_column = 0
        self.sudden_death_offset = 0

        # Physics engine
        self.physics_engine_list = []
        self.setup()

    def setup(self, cup=None):
        # Reset game information
        self.player_dict = {}
        self.game_over = False
        self.time_taken = 0

        # Reset sudden death
        self.sudden_death_in_progress = False
        self.sudden_death_timer = 0
        self.sudden_death_in_progress = False
        self.sudden_death_row = 0
        self.sudden_death_column = 0
        self.sudden_death_offset = 0

        # Cup settings
        if cup is not None:
            self.cup = cup
        else:
            self.cup = Cup(settings.num_of_rounds)

        # Initialize sprite lists
        self.player_list = arcade.SpriteList()
        self.all_bombs_list = arcade.SpriteList(use_spatial_hash=True)
        self.explosion_list = arcade.SpriteList(use_spatial_hash=True)
        self.item_list = arcade.SpriteList()

        # Set up the players
        for number in range(2):
            player_sprite = Player(number)
            self.player_list.append(player_sprite)
            self.player_dict[number] = player_sprite

        # Set up the arena
        self.arena = Arena(config.SIZE, settings.arena_name)
        self.arena.setup(self.player_list)

        # Set up the engines
        for player in self.player_list:
            physics_engine = arcade.PhysicsEngineSimple(player, player.collision_list)
            self.physics_engine_list.append(physics_engine)

    def bomb_placement(self, player_num):

        # Check how many bombs has player available VS how many of his bombs are currently placed in arena
        if self.player_list[player_num].bombs_available <= len(self.player_list[player_num].bomb_list):
            return

        # If player collides with any bomb (means that player have placed bomb recently and haven't moved since then)
        # he can't be able to place a new bomb
        hit_list_bomb = arcade.check_for_collision_with_list(self.player_list[player_num], self.all_bombs_list)
        if len(hit_list_bomb) > 0:
            return

        # If player collides with any explosion he can't place bombs
        hit_list_explosion = arcade.check_for_collision_with_list(self.player_list[player_num], self.explosion_list)
        if len(hit_list_explosion) > 0:
            return

        # Bomb destination calculation
        hit_list_field = arcade.check_for_collision_with_list(self.player_list[player_num], self.arena.field_list)
        destination = (arcade.get_closest_sprite(self.player_list[player_num], hit_list_field))[0]

        # Check of bomb presence
        if destination.bomb_presence:
            return
        else:
            destination.bomb_presence = True

        # Bomb placement
        bomb_sprite = Bomb(destination.row, destination.column, self.player_list[player_num].flame_length)
        self.player_list[player_num].bomb_list.append(bomb_sprite)
        self.all_bombs_list.append(bomb_sprite)

        self.player_list[player_num].placing_bomb = True

    def bombs_countdown(self):
        for player in self.player_list:
            if player is not None:
                for bomb in player.bomb_list:
                    bomb.timer -= 250
                    if bomb.timer <= 0:
                        self.arena.fields[bomb.row][bomb.column].bomb_presence = False
                        for ex in self.arena.calculate_explosion_radius(bomb):
                            self.explosion_list.append(ex)
                        bomb.kill()

    def explosions_countdown(self):
        for explosion in self.explosion_list:
            explosion.timer -= 400
            if explosion.timer <= 0:
                explosion.kill()
                if self.arena.fields[explosion.row][explosion.column].item_presence:
                    item = self.arena.fields[explosion.row][explosion.column].item
                    self.item_list.append(item)

    def sudden_death_countdown(self):
        self.sudden_death_timer -= 500
        if self.sudden_death_timer <= 0:
            # Arena method call for creating new solid block
            self.arena.create_solid_block(self.sudden_death_row, self.sudden_death_column, self.player_list)

            # Check for collision with players, bombs, items and bricks and destroying them
            hit_solid_player_list = arcade.check_for_collision_with_list(
                self.arena.fields[self.sudden_death_row][self.sudden_death_column], self.player_list)
            for hit in hit_solid_player_list:
                hit.kill()
            hit_solid_bomb_list = arcade.check_for_collision_with_list(
                self.arena.fields[self.sudden_death_row][self.sudden_death_column], self.all_bombs_list)
            for hit in hit_solid_bomb_list:
                hit.kill()
            hit_solid_item_list = arcade.check_for_collision_with_list(
                self.arena.fields[self.sudden_death_row][self.sudden_death_column], self.item_list)
            for hit in hit_solid_item_list:
                hit.kill()
            hit_solid_brick_list = arcade.check_for_collision_with_list(
                self.arena.fields[self.sudden_death_row][self.sudden_death_column], self.arena.brick_list)
            for hit in hit_solid_brick_list:
                hit.kill()

            # Next solid block location calculation
            if self.sudden_death_column < config.COLUMN_COUNT - 1 - self.sudden_death_offset and self.sudden_death_row < 2:
                self.sudden_death_column += 1
            elif self.sudden_death_row < config.ROW_COUNT - 1 - self.sudden_death_offset and self.sudden_death_column > config.COLUMN_COUNT - 3:
                self.sudden_death_row += 1
            elif self.sudden_death_column > self.sudden_death_offset and self.sudden_death_row > config.ROW_COUNT - 3:
                self.sudden_death_column -= 1
            elif self.sudden_death_row > self.sudden_death_offset and self.sudden_death_column < 3:
                self.sudden_death_row -= 1

            # Change of offset when starting 2nd spiral
            if self.sudden_death_row == 1 and self.sudden_death_column == 1:
                self.sudden_death_offset = 1

            # Restart timer
            self.sudden_death_timer = config.SUDDEN_DEATH_DURATION

    def check_game_over(self):
        game_over_status = False
        if len(self.player_list) <= 1 or self.time_taken >= config.ROUND_TIME_LIMIT:
            game_over_status = True
        return game_over_status

    def set_winner(self):
        winner = None
        if len(self.player_list) == 1:
            winner = self.player_list.pop()
        return winner

    def on_update(self, delta_time):
        self.time_taken += delta_time
        self.game_over = self.check_game_over()

        for player in self.player_list:
            if player is not None:
                player.update(self.arena.field_list)

                # Player kill
                hit_player_explosion_list = arcade.check_for_collision_with_list(player, self.explosion_list)
                if len(hit_player_explosion_list) > 0:
                    self.player_dict[player.num] = None
                    player.kill()
                    break

                # Picking item and applying its power
                hit_player_item_list = arcade.check_for_collision_with_list(player, self.item_list)
                for item in hit_player_item_list:
                    item.sound.play(config.SOUND_ITEM_GET_VOLUME)
                    if isinstance(item, BombUp) and player.bombs_available < config.PLAYER_DEFAULT_MAX_BOMBS_AVAILABLE:
                        player.bombs_available += 1
                    elif isinstance(item, Flame) and player.flame_length < config.PLAYER_DEFAULT_MAX_FLAME_LENGTH:
                        player.flame_length += 1
                    elif isinstance(item, Skate) and player.speed < config.PLAYER_DEFAULT_MAX_SPEED:
                        player.speed += 0.5

                    self.arena.fields[item.row][item.column].item_presence = False
                    self.arena.fields[item.row][item.column].item = None
                    item.kill()

                # Adding bombs to player's collision_list logic
                if len(self.all_bombs_list) > 0:
                    # Add enemy's bombs with which player doesn't collide to his collision_list
                    for enemy in self.player_list:
                        if enemy is not None and enemy is not player and len(enemy.bomb_list) > 0:
                            for bomb in enemy.bomb_list:
                                if not arcade.check_for_collision(player, bomb) and bomb not in player.collision_list:
                                    player.collision_list.append(bomb)
                    # Check whether player moved away from his last bomb and if so add it to his collision_list
                    if player.placing_bomb and len(player.bomb_list) > 0:
                        if not arcade.check_for_collision(player, player.bomb_list[-1]):
                            player.placing_bomb = False
                            player.collision_list.append(player.bomb_list[-1])

        # Brick with Explosion collisions
        for ex in self.explosion_list:
            hit_list = arcade.check_for_collision_with_list(ex, self.arena.brick_list)
            for brick in hit_list:
                brick.kill()
                self.arena.fields[brick.row][brick.column].brick = None

        # Bomb with Explosion collisions
        for bomb in self.all_bombs_list:
            if arcade.check_for_collision_with_list(bomb, self.explosion_list):
                bomb.timer = 0

        # Item with Explosion collisions
        for item in self.item_list:
            if arcade.check_for_collision_with_list(item, self.explosion_list):
                self.arena.fields[item.row][item.column].item_presence = False
                self.arena.fields[item.row][item.column].item = None
                item.kill()

        # Check for sudden death start
        if config.ROUND_TIME_LIMIT - self.time_taken <= config.SUDDEN_DEATH_START_TIME and not self.sudden_death_in_progress:
            self.sudden_death_in_progress = True
            self.sudden_death_timer = config.SUDDEN_DEATH_DURATION
            # TODO - whistle sound

        # Countdowns
        self.bombs_countdown()
        self.explosions_countdown()
        if self.sudden_death_in_progress:
            self.sudden_death_countdown()

        # Animations
        self.all_bombs_list.update_animation()
        self.explosion_list.update_animation()
        self.player_list.update_animation()

        for physics_engine in self.physics_engine_list:
            physics_engine.update()


class Settings:
    def __init__(self, game_mode, num_of_rounds, map_name, number_of_players=2):
        self.number_of_players = number_of_players
        self.game_mode = game_mode
        self.num_of_rounds = num_of_rounds
        self.arena_name = map_name

    def set_number_of_players(self, number_of_players):
        self.number_of_players = number_of_players

    def set_num_of_rounds(self, num_of_rounds):
        self.num_of_rounds = num_of_rounds

    def set_game_mode(self, game_mode):
        self.game_mode = game_mode

    def set_arena_name(self, arena_name):
        self.arena_name = arena_name


class GameResult:
    def __init__(self, winner, time_taken, cup_result=False):
        self.winner = winner
        self.cup_result = cup_result
        self.time_taken = time_taken


class Cup:
    def __init__(self, num_of_rounds):
        self.num_of_rounds = num_of_rounds
        self.results_dict = {"red": 0, "blue": 0}
        self.time_taken = 0
        self.in_progress = True
