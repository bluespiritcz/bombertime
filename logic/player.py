import arcade

import config
from logic.arena import Field


def load_texture_pair(filename):
    """
    Load a texture pair, with the second being a mirror image.
    """
    return [
        arcade.load_texture(filename),
        arcade.load_texture(filename, mirrored=True)
    ]


class Player(arcade.Sprite):
    def __init__(self, number):
        super().__init__()

        self.num = number

        # Index specific settings
        self.main_path = config.PLAYER_ASSETS_FOLDER[self.num]
        self.player_color = config.PLAYER_COLOR[self.num]
        self.row = config.PLAYER_START_ROW[self.num]
        self.column = config.PLAYER_START_COL[self.num]

        # Skills
        self.speed = config.PLAYER_DEFAULT_SPEED
        self.flame_length = config.PLAYER_DEFAULT_FLAME_LENGTH
        self.bombs_available = config.PLAYER_DEFAULT_BOMBS_AVAILABLE

        # Movement and actions
        self.up_pressed = False
        self.down_pressed = False
        self.left_pressed = False
        self.right_pressed = False
        self.placing_bomb = False

        # Default to face-right
        self.character_face_direction = config.PLAYER_RIGHT_FACING
        # Used for flipping between image sequences
        self.cur_texture = 0

        # List of all currently existing bombs placed by player
        self.bomb_list = arcade.SpriteList(use_spatial_hash=True)
        # List of all collision blocks connected to player
        self.collision_list = arcade.SpriteList(use_spatial_hash=True)

        """Set textures"""
        # Load textures for idle standing
        self.idle_texture_pair = load_texture_pair(f"{self.main_path}idle.png")

        # Load textures for walking
        self.walk_textures = []
        for i in range(2):
            texture = load_texture_pair(f"{self.main_path}walk_{i}.png")
            self.walk_textures.append(texture)

        # Set the initial texture
        self.texture = self.idle_texture_pair[0]
        self.scale = config.PLAYER_SCALING

        # Position
        self.set_position(self.row, self.column)
        self.set_hit_box([[-50, -75], [50, -75], [50, 25], [-50, 25]])

    def set_position(self, row, column):
        center_coordinates = Field.calculate_center_coordinates(row, column)
        self.center_x = center_coordinates[0]
        self.center_y = center_coordinates[1] + config.SIZE * 0.25

    def update(self, fields_occupied_by_player_list):
        self.change_x = 0
        self.change_y = 0

        if self.up_pressed and not self.down_pressed:
            self.change_y = self.speed * config.SIZE * 0.012
        elif self.down_pressed and not self.up_pressed:
            self.change_y = -self.speed * config.SIZE * 0.012
        if self.left_pressed and not self.right_pressed:
            self.change_x = -self.speed * config.SIZE * 0.012
        elif self.right_pressed and not self.left_pressed:
            self.change_x = self.speed * config.SIZE * 0.012

        # Sliding corners logic
        T = (arcade.get_closest_sprite(self, fields_occupied_by_player_list))[0]
        # If player moves towards the closest field (T) he moves normally,
        # if not there comes the sliding mechanic
        if T is not None and T.row % 2 == 0 and T.column % 2 == 0:
            if self.change_x == 0:
                delta_x = abs(T.center_x - self.center_x)
                if T.center_x < self.center_x and delta_x > 5:
                    if self.change_y != 0:
                        self.change_x = -self.speed
                elif T.center_x > self.center_x and delta_x > 5:
                    if self.change_y != 0:
                        self.change_x = self.speed
            elif self.change_y == 0:
                delta_y = abs(T.center_y - (self.top - 50 * config.PLAYER_SCALING))
                if T.center_y < self.top - 50 * config.PLAYER_SCALING and delta_y > 5:
                    if self.change_x != 0:
                        self.change_y = -self.speed
                elif T.center_y > self.top - 50 * config.PLAYER_SCALING and delta_y > 5:
                    if self.change_x != 0:
                        self.change_y = self.speed

        # Top border collision
        if self.top >= config.SCREEN_HEIGHT - config.SCOREBOARD_SIZE - 1 and self.change_y > 0:
            self.change_y = 0
        # Bottom border collision
        if self.bottom <= 0 and self.change_y < 0:
            self.change_y = 0
        # Left border collision
        if self.left <= 0 and self.change_x < 0:
            self.change_x = 0
        # Right border collision
        if self.right > config.SCREEN_WIDTH - 1 and self.change_x > 0:
            self.change_x = 0

    def update_animation(self, delta_time: float = 1 / 60):
        # Figure out if we need to flip face left or right
        if self.change_x < 0 and self.character_face_direction == config.PLAYER_RIGHT_FACING:
            self.character_face_direction = config.PLAYER_LEFT_FACING
        elif self.change_x > 0 and self.character_face_direction == config.PLAYER_LEFT_FACING:
            self.character_face_direction = config.PLAYER_RIGHT_FACING

        # Walking animation
        if abs(self.change_y) > 0 or abs(self.change_x) > 0:
            self.cur_texture += 1
            if self.cur_texture > config.PLAYER_UPDATES_PER_FRAME:
                self.cur_texture = 0
            self.texture = self.walk_textures[self.cur_texture // config.PLAYER_UPDATES_PER_FRAME][
                self.character_face_direction]
            return

        # Idle animation
        if self.change_x == 0:
            self.texture = self.idle_texture_pair[self.character_face_direction]