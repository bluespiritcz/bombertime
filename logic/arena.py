import random
from abc import ABC

import arcade
import config


class Arena:
    def __init__(self, size, arena_name):
        # List of all fields on the board
        self.field_list = None
        self.solid_list = None
        self.brick_list = None
        self.collision_list = None

        # Sets arena name
        self.arena_name = arena_name

        # Sets size of the fields (square)
        self.size = size

        # Prepare empty grid with appropriate number of rows/columns
        self.fields = [[0] * config.COLUMN_COUNT for i in range(config.ROW_COUNT)]

        # Solution for sound bug (1st sound played freezes game)
        self.sound = config.SOUND_BOMB_PLACEMENT
        self.sound.play(0)

    def setup(self, player_list):
        self.field_list = arcade.SpriteList()
        self.solid_list = arcade.SpriteList()
        self.brick_list = arcade.SpriteList()
        self.collision_list = arcade.SpriteList(use_spatial_hash=True)

        # Arena FIELDS (all)
        current_y = config.SCREEN_HEIGHT - config.SCOREBOARD_SIZE + self.size
        for row in range(config.ROW_COUNT):
            current_y -= self.size
            current_x = 0
            for column in range(config.COLUMN_COUNT):
                # Field creation
                field = Grass(row, column)
                # Field index creation for better lookup
                self.fields[row][column] = field
                # Add to SpriteList
                self.field_list.append(field)
                # Offset
                current_x += self.size

        self.load_from_file(player_list)

    def load_from_file(self, player_list):
        f = open("resources/arenas/" + str(self.arena_name) + ".txt", "r")
        if f.mode == 'r':
            for row in range(config.ROW_COUNT):
                line = f.readline()
                for column in range(config.COLUMN_COUNT):
                    if line[column] != "-":
                        # Loading solid block (indestructible)
                        if line[column].lower() == "x":
                            solid = Solid(row, column)
                            self.fields[row][column].solid = solid
                            self.solid_list.append(solid)
                            self.collision_list.append(solid)
                            for player in player_list:
                                player.collision_list.append(solid)
                        # Loading brick block (destructible)
                        elif line[column].lower() == "b":
                            brick = Brick(row, column)
                            self.fields[row][column].brick = brick
                            self.fields[row][column].load_item()
                            self.brick_list.append(brick)
                            self.collision_list.append(brick)
                            for player in player_list:
                                player.collision_list.append(brick)

    def calculate_explosion_radius(self, bomb):
        i = 1

        # Flame limitations
        up_end = False
        down_end = False
        right_end = False
        left_end = False

        explosion_radius_list = arcade.SpriteList()
        center_explosion = Explosion(bomb.row, bomb.column)

        explosion_radius_list.append(center_explosion)

        while i <= bomb.flame_length:
            # Checks all directions for window border or existence of Solid block
            if bomb.row - i < 0 or (self.fields[bomb.row - i][bomb.column].solid is not None):
                up_end = True
            if bomb.row + i >= config.ROW_COUNT or (self.fields[bomb.row + i][bomb.column].solid is not None):
                down_end = True
            if bomb.column + i >= config.COLUMN_COUNT or (self.fields[bomb.row][bomb.column + i].solid is not None):
                right_end = True
            if bomb.column - i < 0 or (self.fields[bomb.row][bomb.column - i].solid is not None):
                left_end = True

            # Creates Explosions and sets directional end if on the same field exists Brick block or another Bomb
            if not up_end:
                up_explosion = Explosion(bomb.row - i, bomb.column)
                explosion_radius_list.append(up_explosion)
                if (self.fields[bomb.row - i][bomb.column].brick is not None) or (
                        self.fields[bomb.row - i][bomb.column]).bomb_presence:
                    up_end = True
            if not down_end:
                down_explosion = Explosion(bomb.row + i, bomb.column)
                explosion_radius_list.append(down_explosion)
                if (self.fields[bomb.row + i][bomb.column].brick is not None) or (
                        self.fields[bomb.row + i][bomb.column]).bomb_presence:
                    down_end = True
            if not right_end:
                right_explosion = Explosion(bomb.row, bomb.column + i)
                explosion_radius_list.append(right_explosion)
                if (self.fields[bomb.row][bomb.column + i].brick is not None) or (
                        self.fields[bomb.row][bomb.column + i]).bomb_presence:
                    right_end = True
            if not left_end:
                left_explosion = Explosion(bomb.row, bomb.column - i)
                explosion_radius_list.append(left_explosion)
                if (self.fields[bomb.row][bomb.column - i].brick is not None) or (
                        self.fields[bomb.row][bomb.column - i]).bomb_presence:
                    left_end = True

            i += 1

        center_explosion.sound.play(config.SOUND_BOMB_EXPLOSION_VOLUME)
        return explosion_radius_list

    def create_solid_block(self, row, column, player_list):
        """Creates new solid block at specific coordinates"""
        field = self.fields[row][column]
        # If there is already a solid block skip it
        if field.solid is not None:
            return
        else:
            # Creating new solid block
            field.solid = Solid(row, column)
            self.solid_list.append(field.solid)
            self.collision_list.append(field.solid)

            # Add new solid block to player's collision_lists
            for player in player_list:
                player.collision_list.append(field.solid)

            # Sound
            self.sound = config.SOUND_SUDDEN_DEATH_SOLID_DROP
            self.sound.play(config.SOUND_SUDDEN_DEATH_SOLID_DROP_VOLUME)

    def draw(self):
        self.field_list.draw()  # default grass fields
        self.solid_list.draw()  # indestructible collision blocks
        self.brick_list.draw()  # destructible collision blocks


class Field(arcade.AnimatedTimeBasedSprite):
    def __init__(self, row, column):
        super().__init__()

        self.row = row
        self.column = column

        self.center_coordinates = self.calculate_center_coordinates(row, column)
        self.center_x = self.center_coordinates[0]
        self.center_y = self.center_coordinates[1]

    @staticmethod
    def calculate_center_coordinates(row, column):
        center_x = column * config.SIZE + config.SIZE / 2
        # subtraction from SCREEN_HEIGHT is needed due to inverted coordinate system ([0,0] is left down corner)
        center_y = config.SCREEN_HEIGHT - config.SCOREBOARD_SIZE - (row * config.SIZE + config.SIZE / 2)
        return center_x, center_y


class Grass(Field):
    def __init__(self, row, column):
        super().__init__(row, column)
        self.textures.append(config.FIELD_GRASS)
        self.set_texture(0)
        self.scale = config.SCALING

        self.solid = None
        self.brick = None
        self.item = None

        self.item_presence = False
        self.bomb_presence = False

    def load_item(self):
        self.item_presence = random.choice(config.ITEM_OCCURRENCE_PROBABILITY)
        if self.item_presence:
            # Gets random type of item
            item_type = random.choice(config.ITEM_LIST)

            # Creates item of previously chosen type
            if item_type == "flame":
                item = Flame(self.row, self.column)
            elif item_type == "bomb_up":
                item = BombUp(self.row, self.column)
            elif item_type == "skate":
                item = Skate(self.row, self.column)

            # Assign chosen item to Grass field attribute item
            self.item = item


class Solid(Field):
    def __init__(self, row, column):
        super().__init__(row, column)
        self.textures.append(config.FIELD_SOLID)
        self.set_texture(0)
        self.scale = config.SCALING


class Brick(Field):
    def __init__(self, row, column):
        super().__init__(row, column)
        self.textures.append(config.FIELD_BRICK)
        self.set_texture(0)
        self.scale = config.SCALING


class Bomb(Field):
    def __init__(self, row, column, flame_length):
        super().__init__(row, column)

        self.flame_length = flame_length
        self.timer = config.BOMB_TICKING_DURATION

        # Textures
        self.textures.append(config.BOMB)
        self.textures.append(config.BOMB_LIT)
        self.set_texture(0)
        self.scale = config.BOMB_SCALING

        # Animation
        self.frames.append(arcade.AnimationKeyframe(0, 250, self.textures[0]))
        self.frames.append(arcade.AnimationKeyframe(1, 250, self.textures[1]))

        # Sound
        self.sound = config.SOUND_BOMB_PLACEMENT
        self.sound.play(config.SOUND_BOMB_PLACEMENT_VOLUME)


class Explosion(Field):
    def __init__(self, row, column):
        # Set up parent class
        super().__init__(row, column)

        self.timer = config.EXPLOSION_DURATION

        # Textures
        self.textures.append(config.FLAME)
        self.textures.append(config.FLAME_LIT)
        self.set_texture(0)
        self.scale = config.FLAME_SCALING

        # Animation
        self.frames.append(arcade.AnimationKeyframe(0, 400, self.textures[0]))
        self.frames.append(arcade.AnimationKeyframe(1, 400, self.textures[1]))

        # Sound
        self.sound = config.SOUND_BOMB_EXPLOSION


class Item(Field):
    def __init__(self, row, column):
        super().__init__(row, column)

        self.scale = config.ITEM_SCALING
        self.alpha = 255

        # Sound
        self.sound = config.SOUND_ITEM_GET


class BombUp(Item):
    def __init__(self, row, column):
        super().__init__(row, column)

        self.textures.append(config.ITEM_BOMB_UP)
        self.set_texture(0)


class Skate(Item):
    def __init__(self, row, column):
        super().__init__(row, column)

        self.textures.append(config.ITEM_SKATE)
        self.set_texture(0)


class Flame(Item):
    def __init__(self, row, column):
        super().__init__(row, column)

        self.textures.append(config.ITEM_FLAME)
        self.set_texture(0)
