import os
import sys

import arcade
import pyglet

if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    os.chdir(sys._MEIPASS)

# Game arena
ROW_COUNT = 9
COLUMN_COUNT = 13

# Window/screen
SCREEN_TITLE = "BomberTime"
MONITOR_ID = 0  # default 0

MAX_DIMENSIONS = arcade.get_display_size(MONITOR_ID)
SIZE = (MAX_DIMENSIONS[1] - 200) // ROW_COUNT

SCOREBOARD_SIZE = SIZE
SCREEN_WIDTH = SIZE * COLUMN_COUNT
SCREEN_HEIGHT = (SIZE * ROW_COUNT) + SCOREBOARD_SIZE

# Controls
PLAYER_CONTROLS_UP = [arcade.key.W, arcade.key.UP]
PLAYER_CONTROLS_DOWN = [arcade.key.S, arcade.key.DOWN]
PLAYER_CONTROLS_LEFT = [arcade.key.A, arcade.key.LEFT]
PLAYER_CONTROLS_RIGHT = [arcade.key.D, arcade.key.RIGHT]
PLAYER_CONTROLS_BOMB_PLACEMENT = [arcade.key.SPACE, arcade.key.ENTER]

# Times
SUDDEN_DEATH_START_TIME = 120  # default 120
ROUND_TIME_LIMIT = 240  # default 240
BOMB_TICKING_DURATION = 50000  # default 50000
EXPLOSION_DURATION = 16000  # default 16000
SUDDEN_DEATH_DURATION = 15000  # default 15000

# Scaling
SCALING = SIZE / 128
BOMB_SCALING = SIZE / 70
BOMB_SCALING_LIT = (SIZE - 30) / 44
FLAME_SCALING = BOMB_SCALING
ITEM_SCALING = SIZE / 65
PLAYER_ICON_SCALING = SIZE / 100 + (SIZE / 100 / 5)
ARENA_PREVIEW_SCALING = 0.3

# Textures
FIELD_GRASS = arcade.load_texture("resources/images/tiles/field_grass.png")
FIELD_SOLID = arcade.load_texture("resources/images/tiles/field_solid.png")
FIELD_BRICK = arcade.load_texture("resources/images/tiles/field_brick.png")

BOMB = arcade.load_texture("resources/images/tiles/bomb.png")
BOMB_LIT = arcade.load_texture("resources/images/tiles/bomb_lit.png")
FLAME = arcade.load_texture("resources/images/tiles/flame.png")
FLAME_LIT = arcade.load_texture("resources/images/tiles/flame_lit.png")

ITEM_FLAME = arcade.load_texture("resources/images/items/powerup_flame.png")
ITEM_BOMB_UP = arcade.load_texture("resources/images/items/powerup_bomb_up.png")
ITEM_SKATE = arcade.load_texture("resources/images/items/powerup_skate.png")

# Players
PLAYER_COLOR = ["red", "blue"]
PLAYER_ASSETS_FOLDER = ["resources/images/player/red/", "resources/images/player/blue/"]
PLAYER_IDLE = "idle.png"
PLAYER_IDLE = "hurt.png"
PLAYER_ICON = "badge.png"
PLAYER_SCALING = (SIZE - 10) / 100

PLAYER_START_ROW = [0, 0]
PLAYER_START_COL = [0, COLUMN_COUNT - 1]

PLAYER_DEFAULT_BOMBS_AVAILABLE = 1  # default 1
PLAYER_DEFAULT_FLAME_LENGTH = 1  # default 1
PLAYER_DEFAULT_SPEED = 3  # default 3

PLAYER_DEFAULT_MAX_BOMBS_AVAILABLE = 8  # default 8
PLAYER_DEFAULT_MAX_FLAME_LENGTH = 8  # default 8
PLAYER_DEFAULT_MAX_SPEED = 8  # default 8

PLAYER_RIGHT_FACING = 0
PLAYER_LEFT_FACING = 1
PLAYER_UPDATES_PER_FRAME = 7

PLAYER_ICON_CENTER_X = [SIZE * 1.5, SCREEN_WIDTH - (SIZE * 1.5)]
PLAYER_ICON_CENTER_Y = SCREEN_HEIGHT - (SCOREBOARD_SIZE // 2)

# Game settings
NUM_OF_ROUNDS_LIST = [1, 3, 5, 7]

# Power-ups
ITEM_LIST = ["flame", "bomb_up", "skate"]
ITEM_OCCURRENCE_PROBABILITY = [True, False, False]

# Fonts
MAIN_FONT_PATH = "resources/fonts/PressStart2P-vaV7.ttf"
pyglet.font.add_file(MAIN_FONT_PATH)
MAIN_FONT_NAME = "Press Start 2P"
SCOREBOARD_CONTROLS_FONT_SIZE = SIZE // 10
SCOREBOARD_FONT_SIZE = SIZE // 8
SCOREBOARD_FONT_SIZE_BIGGER = SIZE // 4

# Scoreboard visuals
SCOREBOARD_CONTROLS_START_X = [SIZE // 8, SCREEN_WIDTH - (SIZE // 8)]
SCOREBOARD_CONTROLS_START_Y = SCREEN_HEIGHT - (SCOREBOARD_SIZE * 0.4)
SCOREBOARD_CONTROLS_ANCHOR_X = ["left", "right"]
SCOREBOARD_TIMER_START_X = SCREEN_WIDTH // 2
SCOREBOARD_TIMER_START_Y = SCREEN_HEIGHT - (SCOREBOARD_SIZE // 2)
SCOREBOARD_STAT_START_X = [2 * SCREEN_WIDTH // 10, SCREEN_WIDTH - (2 * SCREEN_WIDTH // 10)]
SCOREBOARD_STAT_START_Y = SCREEN_HEIGHT - (SCOREBOARD_SIZE * 0.4)
SCOREBOARD_STAT_SPACING = SIZE // 5
SCOREBOARD_STAT_ANCHOR_X = ["left", "right"]

# Buttons
BTN_DEFAULT_STYLE = dict(font_name=MAIN_FONT_NAME, font_size=SIZE // 5, bg_color=(55, 54, 52))

# Sounds
SOUND_BOMB_PLACEMENT = arcade.load_sound("resources/sounds/bomb_set.wav")
SOUND_BOMB_PLACEMENT_VOLUME = 0.02
SOUND_BOMB_EXPLOSION = arcade.load_sound("resources/sounds/bomb_explosion.mp3")
SOUND_BOMB_EXPLOSION_VOLUME = 0.01
SOUND_SUDDEN_DEATH_SOLID_DROP = arcade.load_sound("resources/sounds/solid_fall.wav")
SOUND_SUDDEN_DEATH_SOLID_DROP_VOLUME = 0.03
SOUND_ITEM_GET = arcade.load_sound("resources/sounds/item_get.wav")
SOUND_ITEM_GET_VOLUME = 0.03
